namespace NthRoot.Tests;

public class NewtonTests
{

    [TestCase(0.9)]
    [TestCase(0.1)]
    [TestCase(0.0)]
    [TestCase(-0.1)]
    [TestCase(-2.0)]
    public void NthRootShouldRejectNumberLowerThanOne(double number)
    {
        var exception = Assert.Throws<ArgumentOutOfRangeException>(() => Newton.NthRoot(number, 2));

        if (exception != null)
        {
            Assert.That(exception.ParamName, Is.EqualTo("number"));
        }
    }

    [TestCase(1.0)]
    [TestCase(1.1)]
    [TestCase(2.0)]
    [TestCase(9.9)]
    public void NthRootShouldAcceptNumberGreaterThanOrEqualToOne(double number)
    {
        Assert.DoesNotThrow(() => Newton.NthRoot(number, 2));
    }

    [TestCase(0)]
    [TestCase(-1)]
    public void NthRootShouldRejectRootLowerThanOne(int root)
    {
        var exception = Assert.Throws<ArgumentOutOfRangeException>(() => Newton.NthRoot(3.3, root));

        if (exception != null)
        {
            Assert.That(exception.ParamName, Is.EqualTo("root"));
        }
    }

    [TestCase(1)]
    [TestCase(2)]
    [TestCase(10)]
    public void NthRootShouldAcceptRootGreaterThanOrEqualToOne(int root)
    {
        Assert.DoesNotThrow(() => Newton.NthRoot(5.5, root));
    }

    [TestCase(100d, 2, 10d)]
    [TestCase(25d, 2, 5d)]
    [TestCase(1000d, 3, 10d)]
    public void NthRootShouldBeCloseToOracle(double number, int root, double expected)
    {
        double actual = Newton.NthRoot(number, root);

        Assert.That(actual, Is.EqualTo(expected).Within(10).Ulps);
    }

    [Test]
    public void NthRootShouldReturnSomethingEvenForQuiteBigNumbers()
    {
        Newton.NthRoot(1E100, 2);
        Newton.NthRoot(1E100, 3);
    }

    [Test]
    public void NthRootShouldFailWithAnExceptionForReallyTooBigNumber()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => Newton.NthRoot(double.MaxValue, 2));
    }

    [Test]
    public void NthRootShouldFailWithAnExceptionForReallyTooBigRoot()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => Newton.NthRoot(1E50, int.MaxValue));
    }
}
