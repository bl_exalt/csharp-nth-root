﻿namespace NthRoot;

// https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Newton#Racine_carr%C3%A9e
public static class Newton
{
    private static readonly int MAX_ITERATION_COUNT = 100;

    public static double NthRoot(double number, int root)
    {
        if (number < 1.0)
        {
            throw new ArgumentOutOfRangeException("number");
        }

        if (root < 1)
        {
            throw new ArgumentOutOfRangeException("root");
        }

        var nthRoot = number;
        var count = 0;
        while (true)
        {
            var step = NthRootStep(number, root, nthRoot);

            nthRoot = nthRoot - step;

            if (double.IsNaN(nthRoot))
            {
                throw new ArgumentOutOfRangeException("number");
            }

            if (count++ > MAX_ITERATION_COUNT)
            {
                return nthRoot;
            }

            // TODO Benoît LABAERE 2023/01/12 this could a constant, or a parameter or computed.
            if (abs(step) < 10 * double.Epsilon)
            {
                return nthRoot;
            }
        }
    }

    private static double NthRootStep(double number, int root, double previous)
    {
        return
            (power(previous, root) - number)
            /
            (root * power(previous, root - 1));
    }


    private static double power(double x, int n)
    {
        double result = 1d;

        for (int i = 0; i < n; i++)
        {
            result *= x;
        }

        return result;
    }

    private static double abs(double x)
    {
        return x < 0.0 ? -x : x;
    }
}
