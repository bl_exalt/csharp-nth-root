# Csharp Cpp JAVA  -  NthRoot

Write a method that returns the nth root of a given positive number.


## Spec

input:
    - number: double
    - root: int

output:
    nth root of number


Assume following

·         number >= +1.00

·         root   >= +1


## Rules:

·         You can only use 4 basic math operations + - * /

·         You cannot use any Math.* methods
